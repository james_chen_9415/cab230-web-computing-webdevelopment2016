<?php



	function validateAll(&$errors, $field_list, $field_name) {

		$patternAll = array('login_user_email' =>"/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/",
				'register_user_email' =>"/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/",
				'user_password' =>  "/^[a-zA-Z0-9!-\.\_]{8,16}$/",
				'register_user_password' =>  "/^[a-zA-Z0-9!-\.\_]{8,16}$/",
				'user_firstname' =>  "/^[a-zA-Z]{2,}$/",
				'user_lastname' =>  "/^[a-zA-Z]{2,}$/",
				'user_birthday' =>  "/^(18|19|20)\d\d[\-\/](0[1-9]|1[012])[\-\/](0[1-9]|[12][0-9]|3[01])$/",   
				'user_telephone' => "/^\d{2}[\-]\d{4}[\-]\d{4}$/"
				);
		
		$pattern = $patternAll[$field_name];

		
		if ( !isset($field_list[$field_name]) || empty($field_list[$field_name]) ){

			if ($field_name == 'login_user_email'){
				$errors[$field_name] = "Email required";
			} else if ($field_name == 'user_password'){ 
				$errors[$field_name] = "Password required";
			}else{
				$errors[$field_name] = "Required";	
			}
					
		}else if (!preg_match($pattern, $field_list[$field_name])){
			if ($field_name == 'login_user_email') 
				$errors[$field_name] = "Email invalid";
			else if ($field_name == 'user_password') 
				$errors[$field_name] = "Password invalid";
			else 
				$errors[$field_name] = "Invalid";
		}
	}

	function checkPasswordRepeat(&$errors, $field_list, $field_name, $field_name2){
	
		if( $field_list[$field_name] != $field_list[$field_name2] ){
			$errors[$field_name2] = "Not matched";
		}
	}



	function validateNotation($data){

		$data = trim($data);
		$data = stripSlashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

?>