<?php 
	include 'sessionStart.inc';
?>

<!DOCTYPE HTML>
<html>
	
	<head>
		<!-- metadata -->
		<meta charset = "UTF-8">
		<meta name="description" content="AUthors of TennisFinder." />
		<meta name="keywords" content="html, css, javascript, php, MySQL, students, QUT, IT, web dvelopment" />
		<meta name="author" content="Renzo Alvarado and Jiaming Chen">
		<meta name="robots" content="noindex, nofollow">
		<title>About Us</title>
		<!-- External CSS -->
		<link href="css/content_about_style.css" rel="stylesheet" type="text/css"/>
		<link href="css/index_style.css" rel="stylesheet" type="text/css"/>
	</head>
	


	<body>
	
		<!-- Contains: Header, ContentSearch and Footer -->
		<div id="wrapper">
			
			<!-- Includes: Logo, loging links and Menu Bar -->
			<?php include 'header.inc';?>

			<div id="contentphoto">
				<img src="img/photo.jpg" alt="team photo">	

				<table>
				<tr>
					<td> <p class="teamName">Jiaming Chen</p> <p class="idnumber">n9370331</p>I love this unit. I learnt a lot about php and MySQL. </td> 
					<td> <p class="teamName">Renzo Alvarado</p> <p class="idnumber">n9696164</p>This project has helped me to understand more about UX.</td>
				</tr>
				</table>
			</div>

			<div id="footer">
				<p>Copyright &copy; 2016 JamZo CAB230 - Queensland University of Technology. All Rights Reserved</p>
			</div>

		</div><!--close wrapper-->

	</body>
</html>