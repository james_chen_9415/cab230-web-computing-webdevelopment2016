<?php 
	include 'sessionStart.inc';
?>

<!DOCTYPE HTML>
<html>

	<head>
		<!-- metadata -->
		<meta charset = "UTF-8">
		<meta name="description" content="Best tennis courts around Brisbane ordered by name, suburb or rating." />
		<meta name="keywords" content="tennis, courts, brisbane, council, play, sports" />
		<meta name="author" content="Renzo Alvarado and Jiaming Chen">
		<meta name="robots" content="noindex, nofollow">
		<title>Results</title>
		<!-- External CSS -->
		<link href="css/index_style.css" rel="stylesheet" type="text/css"/>
		<link href="css/content_results_style.css" rel="stylesheet" type="text/css"/>
		<!-- External JavaScript-->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="javascript/map.js"></script>
	</head>
	
	<body>
	
		<!-- Contains: Header, ContentSearch and Footer -->
		<div id="wrapper">

			<!-- Includes: Logo, loging links and Menu Bar -->
			<?php include 'header.inc';?>
			
			
			<!-- Contains: TopArea and BottomArea-->
			<div id="contentsearch">
				<!-- Contains: TopAreaWrapper-->
				<div id="toparea">
					<!-- Contains: FinderIntro and FinderInput-->
					<div id="topareawrapper">
						<br/><br/>
						<p id="finderintro">

							Find information about tennis courts for community use in Brisbane parks, tennis clubs and schools.
						</p>
							

							<?php 
								// finderinput div
								include 'searchForm.inc';
							?>
						<span id="geolocationInstruction">Near me:<input type="checkbox" name="geolocation" onclick="showMap('mapholder');" ></span>
					</div><!--close topareawrapper-->	
				</div><!--close toparea-->	
				<hr/>
				<!-- Contains: BottomAreaWrapper-->
				<div id="bottomarea">
					<!-- Contains: Sidebar and Results-->
					<div id="bottomareawrapper">
						
						
						
						<div id="results">
							<p class="title">Results</p>





							<?php 

								include 'mysql.connect';

								//query to get data to create table with results of the search
								$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								try { 

									if(isset($_GET['suburb']) && $_GET['suburb']!= 'Select Suburb'){
										$detail = $pdo->query("SELECT items.Venue, Suburb, temp.AvgRating FROM ( SELECT Venue, round(AVG(Rating),0) AS AvgRating FROM reviews group by Venue ) AS temp RIGHT JOIN items ON temp.Venue = items.Venue WHERE items.Suburb='$_GET[suburb]';");
									}else if(isset($_GET['venueName']) && $_GET['venueName']!= '' ){
										$detail = $pdo->query("SELECT items.Venue, Suburb, AvgRating FROM ( SELECT Venue, round(AVG(Rating),0) AS AvgRating FROM reviews group by Venue ) AS temp RIGHT JOIN items ON temp.Venue = items.Venue WHERE items.Venue='$_GET[venueName]';");
									}else if(isset($_GET['rating']) && $_GET['rating']!='Select Rating'){
										$detail = $pdo->query("SELECT * FROM (SELECT reviews.Venue, Suburb, round(AVG(Rating),0) AS AvgRating FROM reviews, items WHERE items.Venue = reviews.Venue GROUP BY Venue) AS t WHERE AvgRating=1;");
									}
									
								} catch (PDOException $e) {
									echo $e->getMessage(); 
								}

							?>

							<table id="result-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Suburb</th>
										<th>Rating</th>
										<!-- <th>Distance</th> -->
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach($detail as $det){
											$venueValue = str_replace(' ', '%20', $det['Venue']);

											echo '<tr>';
											echo "<td><a href=\"individualitem.php?VenueName=$venueValue\">$det[Venue]</a></td> <td>$det[Suburb]</td> <td>$det[AvgRating]</td> ";
											echo '</tr>';
										}
									?>
								</tbody>
							</table>

							<br/><br/>	



							<!-- added  -->
							<div id="MapException"> </div>

							<?php 

								$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								try { 
									$detail = $pdo->query("SELECT Venue, latitude, longitude FROM items;");
								} catch (PDOException $e) {
									echo $e->getMessage(); 
								}

								$locations = array();
								foreach($detail as $det){
									$tempArr = array($det['Venue'], $det['latitude'], $det['longitude']);
									array_push($locations,$tempArr);
								}
								$strLocations = json_encode($locations);

								echo "<script type=\"text/javascript\">getLocation($strLocations);</script>";
							?>
							<div id="mapholder"></div>

							
						</div> <!--close results-->
					
					</div><!--close bottomareawrapper-->
				</div><!--close bottomarea-->
				<p><a href="#logo" class="bookmark">Top of page</a></p>
			</div><!--close contentsearch-->	
			
								
		
			<div id="footer">
				<p>Copyright &copy; 2016 JamZo CAB230 - Queensland University of Technology. All Rights Reserved</p>
			</div>
		
		</div><!--close wrapper-->
	</body>
</html>