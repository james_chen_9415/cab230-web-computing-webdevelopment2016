<?php 
	include 'sessionStart.inc';
?>

<!DOCTYPE HTML>
<html>
	
	<head>
		<!-- metadata -->
		<meta charset = "UTF-8">
		<meta name="description" content="Frequent Answers and questions about TennisFinder." />
		<meta name="keywords" content="html, css, javascript, php, MySQL" />
		<meta name="author" content="Renzo Alvarado and Jiaming Chen">
		<meta name="robots" content="noindex, nofollow">
		<title>FAQ</title>
		<!-- External CSS -->
		<link href="css/index_style.css" rel="stylesheet" type="text/css"/>
		<link href="css/content_faq_style.css" rel="stylesheet" type="text/css"/>
	</head>
	


	<body>
	
		<!-- Contains: Header, ContentSearch and Footer -->
		<div id="wrapper">
			
			<!-- Includes: Logo, loging links and Menu Bar -->
			<?php include 'header.inc';?>

			<div id="contentFAQ">
				
				<p class="question">Can I use these beautiful HTML / CSS templates I found on the web?</p>
				<p>No, you must code your own HTML and CSS.  While many organizations do use template systems, since this is a first web development unit it is important for you to learn how to write this code on your own.</p> 

				<p class="question">Can I use AngularJS / YUI / Bootstrap / favourite HTML/CSS/JavaScript framework here>?</p>
				<p>No.  Web development frameworks are a very dynamic area, and it is very likely that the frameworks that are popular today will be abandoned or obsolete in just a couple of years.  As noted above, it is important for you to learn how to write your own code, and once you have those skills, you will be able to work with appropriate frameworks when the need arises.  (If you have a very good reason to use a framework to do something super awesome in a way that doesn’t defeat the purpose of the assignment, you can propose it to me and I will consider it.) For the maps add-on task, you may include the JavaScript and CSS frameworks from the mapping provider as required.</p>

				<p class="question">Can I use Sass or LESS when I write my CSS code?</p>
				<p>Yes, but it is recommended that you only do so if you have previous experience with CSS and can explain the problems with CSS that Sass or LESS aim to solve.  Be advised that we will only mark your CSS code, so you should ensure that whatever CSS code is generated by Sass or LESS is human-readable and high-quality.  (If you did not understand this question or answer, feel free to ignore it.)</p>

				<p class="question">Can I use jQuery?</p>
				<p>No, you may not use the core jQuery library jQuery UI, jQuery mobile, or any jQuery plugins.  we are  generally trying to avoid the framework-of-the-month approach in CAB230, jQuery is so widespread that it is a de facto standard.  However, if you have been using jQuery for a while, you may be interested to know that newer browsers have much better JavaScript support, making jQuery less of a necessity.  For example, the new document.querySelector(x) JavaScript function does much the same as jQuery’s $(x) function and is fairly widely supported.</p>

				<p class="question">Can I use reset.css / normalize.css?</p>
				<p>You may use a CSS reset stylesheet if you want to have more control over the base formatting of CSS objects.  See http://www.cssreset.com for some possibilities.</p>

				<p class="question">Can I use web font’s services like Google Fonts?</p>
				<p>Yes.  Please see the note above regarding copyright issues.</p>

				<p class="question">Can I use sIFR for displaying fonts?</p>
				<p>No, because sIFR requires Flash and you may not use Flash.</p>

				<p class="question">Can I use an icon font like Font Awesome?</p>
				<p>Yes.</p>

				<p class="question">Can I use a different dataset?</p>
				<p>No.</p>

				<p class="question">Which editor should I use?</p>
				<p>See Blackboard -> Learning Resources -> Textbook(s) and Online Resources for a discussion about text editors.  Note in particular that you may use Adobe Dreamweaver as a text editor, but you should avoid using its WYSISYG / GUI features as they may not output readable code.</p>

				<p class="question">May we use a code repository such as GitHub or BitBucket?</p>
				<p>Yes, however, you need to ensure that the repository is set to private. If any plagiarism occurs due to the use of a repository then standard QUT protocols and penalties will apply.</p>

				<p class="question">Do users need to be able to search by item name and suburb at the same time?</p>
				<p>No. While you need to provide the ability to search by item name, suburb, rating or location each of these options can be performed as a mutually exclusive search.</p>

				<p class="question">Am I able to have the search form and results table on the same page (e.g. like the google search page)?</p>
				<p>Yes, under the following conditions: 1) In stage 1 you need to provide 2 pages - one with an search form and empty results section and one with a search form and sample results data; 2) The search section needs to have an HTML form and the results section needs to have the results presented using a HTML table.</p>  

				<p class="question"> Will the datasets be marked differently?</p> 
				<p> No. All the datasets will be marked the same. Just make sure that you can search by item name, suburb, rating or location. You need to display this information in the individual item pages, as well as the reviews (and map for add on task #1).</p> 
				
				<p class="question"> Can I use MySQLi instead of PDO?</p> 
				<p> No. The main reason is because MySQLi works only with MySQL database, while PDO is independent of the database service used, which means that it works with MySQL, Oracle, SQL server, SQLite and others.</p> 
				
				<p class="question"> Are we allowed to use JSON to pass data around?</p> 
				<p> Yes.</p> 
 				
 				<p class="question"> Do I need to perform both client and server side validation? </p> 
				<p> Yes.</p> 
 
 				<p class="question"> Can we use the email address as the user login name? </p> 
				<p> Yes.</p> 

				<p class="question"> For the search page, can we allow empty searches that return all values </p> 
				<p> Yes  (but you don’t have to and you won’t gain any extra marks if you do).</p> 

				<p class="question"> For the search by location do I need to use a user specified distance? </p> 
 				<p> No. The distance can either be hardcoded or specified by the user.</p> 
  				
  				<p class="question"> Regarding the password storage in the database, can we just use the password_hash function of PHP since the hash it outputs already includes the salt?</p> 
				<p> Yes.</p> 
 
				<p class="question"> How should we implement the search by rating feature? </p> 
				<p> Assuming that the user specifies a rating x, you will receive full marks for this part by:<br/> 
				1.    Returning all items with at least one review with a rating equal to x; or <br/>
				2.    Returning all items with at least one review with a rating equal to or greater than x; or <br/>
				3.    Returning all items with an average rating equal to x across all reviews; or <br/>
				4.    Returning all items with an average rating equal to or greater than x across all reviews; </p>

				<p class="question">I've noticed that a lot of the elements created by Google maps return invalid code. Will we be penalised for this? </p>
				<p>No. This is an issues between the source HTML and the DOM. The source HTML sent by the server was valid, but later the JavaScript altered the DOM which made the HTML invalid. For assessment purposes we will only be checking the source HTML sent by the server.</p>
 				
 				<p class="question">I've found that some of the coordinates in the data set are not correct. What should we do about them?</p>
 				<p>We’re just going to ignore those errors. You can either: 1) return the erroneous locations; 2) change the locations to something more sensible or 3) change the locations to something more sensible.</p>
			
			</div>

			<div id="footer">
				<p>Copyright &copy; 2016 JamZo CAB230 - Queensland University of Technology. All Rights Reserved</p>
			</div>

		</div><!--close wrapper-->

	</body>
</html>