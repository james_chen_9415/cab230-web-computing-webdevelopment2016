<?php 
	include 'sessionStart.inc';
?>

<!DOCTYPE HTML>

<html>
	
	<head>
		<!-- metadata -->
		<meta charset = "UTF-8">
		<meta name="description" content="User registration in TennisFinder." />
		<meta name="keywords" content="tennis, courts, brisbane, council, play, sports" />
		<meta name="author" content="Renzo Alvarado and Jiaming Chen">
		<meta name="robots" content="noindex, nofollow">
		<title>Register</title>
		<!-- External CSS -->
		<link href="css/index_style.css" rel="stylesheet" type="text/css"/>
		<link href="css/content_register_style.css" rel="stylesheet" type="text/css"/>
		<!-- External Scripts -->
		<script src="javascript/registration.js"></script>
	</head>
	
	<body>
	
	<?php //include 'mysql.connect' ?>
		
		<!-- Contains: Header, ContentRegister and Footer -->
		<div id="wrapper">
			

			<!-- Includes: Logo, loging links and Menu Bar -->
			<?php include 'header.inc';?>

			
			<!--Contains: Log in box and Regitration box-->
			<div id="contentRegistration">
					<!-- Form to log in -->
					<div id="loginBox">
						<p class="title">Log In</p>
						<br>

								<?php
									$alarm='';
									$loginErrors = array();
									
									if (isset($_POST['login_user_email']) || isset($_POST['user_password'])){
										require_once 'validate.inc';
										validateAll($loginErrors, $_POST, 'login_user_email');
										validateAll($loginErrors, $_POST, 'user_password');

										if ($loginErrors) {
											include 'loginForm.inc';
										}else{
											// Check whether it is in the database
											// if it in the database, then proceed to login
											// if not, then show user exception
											include 'mysql.connect';
											$stmt = $pdo->prepare('SELECT Email, Salt, Password FROM members WHERE Email = :login_email;');
											$stmt->bindValue(':login_email', $_POST['login_user_email']);
											$stmt->execute();

											$result = $stmt->fetch();

											if($result[0] == $_POST['login_user_email'] && $result[2] == md5($_POST['user_password'].$result[1])){
												//echo "<h2>Login successfully</h2>";
												//session_start();
												// Store Session Data
												$_SESSION['valid'] = true;
												$_SESSION['timeout'] = time();
												$_SESSION['username'] = $result[0];
												echo '<script>window.location.href="index.php"</script>';
											}
												else{
													include 'loginForm.inc';
													$alarm = '<script>alert("Sorry, you e-mail address or password must be wrong");</script>';
												}
											}
									}else{
										include 'loginForm.inc';
									}
								?>

						<br/><br/><br/>
						<p>If you are not registered yet, please fill the following section:</p>

					</div><!-- Close login-box -->
							
					<!-- Form to register -->
					<div id="registerBox">
						<br><br><br><br><br>
						<p class="title">Registration</p>

						<?php

							$RegisterErrors = array();
							
							if (isset($_POST['register_user_email'])){
								include 'validate.inc';
								validateAll($RegisterErrors, $_POST, 'user_firstname');
								validateAll($RegisterErrors, $_POST, 'user_lastname');
								validateAll($RegisterErrors, $_POST, 'user_birthday');
								validateAll($RegisterErrors, $_POST, 'user_telephone');
								validateAll($RegisterErrors, $_POST, 'register_user_email');
								validateAll($RegisterErrors, $_POST, 'register_user_password');
								checkPasswordRepeat($RegisterErrors, $_POST, 'register_user_password', 'user_password_repeat');

								if ($RegisterErrors) {
									include 'registerForm.inc';
								}else{
									// Check whether the account was registered before
									$stmt = $pdo->prepare('SELECT Email, Salt, Password FROM members WHERE Email = :register_email;');
									$stmt->bindValue(':register_email', $_POST['register_user_email']);
									$stmt->execute();	

									if($stmt->rowCount()>0){
										include 'registerForm.inc';
										echo '<script>alert("This email has already been registered.");</script>';
									}else{
										// perpare to insert new account
										$uniqidSalt = uniqid(); 

										$stmt = $pdo->prepare('INSERT INTO members (Email, FirstName,LastName,DateOfBirth,Gender,Telephone, Salt,Password) 
											VALUES (:email,:firstname,:lastname,:birthday, :gender,:telephone, :salt, :password);');
										$stmt->bindValue(':email', $_POST['register_user_email']);
										$stmt->bindValue(':firstname', $_POST['user_firstname']);
										$stmt->bindValue(':lastname', $_POST['user_lastname']);
										$stmt->bindValue(':birthday', $_POST['user_birthday']);
										$stmt->bindValue(':gender', $_POST['user_gender']);
										$stmt->bindValue(':telephone', $_POST['user_telephone']);
										$stmt->bindValue(':salt', $uniqidSalt);
										$stmt->bindValue(':password', md5($_POST['register_user_password'].$uniqidSalt));

										$stmt->execute();
										echo '<h2>Your registration was successful.<h2>'; 
									}
								}
							}else{
								include 'registerForm.inc';
							}
						?>

					</div><!-- Close Registrer Box-->
					<!-- <p><a href="#logo" class="bookmark">Top of page</a></p> -->		
			</div> <!-- Close Registration -->
	
			<br>
			
			<div id="footer">
				<p>Copyright &copy; 2016 JamZo CAB230 - Queensland University of Technology. All Rights Reserved</p>
			</div>
		</div><!--close wrapper-->

		<?php 
			if($alarm!=''){
				echo $alarm;
				$alarm = '';
			}
		?>
	</body>
</html>