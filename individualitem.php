<?php 
	include 'sessionStart.inc';
?>

<!DOCTYPE HTML>
<html>
	
	<head>
		<!-- metadata -->
		<meta charset = "UTF-8">
		<meta name="description" content="Results of searching tennis courts in Brisbane." />
		<meta name="keywords" content="tennis, courts, brisbane, council, play, sports" />
		<meta name="author" content="Renzo Alvarado and Jiaming Chen">
		<meta name="robots" content="noindex, nofollow">
		<title>Item</title>
		<!-- External CSS -->
		<link href="css/index_style.css" rel="stylesheet" type="text/css"/>
		<link href="css/content_individualitem_style.css" rel="stylesheet" type="text/css"/>
		<!-- External Scripts-->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script src="javascript/individualitem-map.js"></script>
	</head>
	
	<body>
		<?php include 'mysql.connect' ?>
	
		<!-- Contains: Header, Content Individual Item and Footer -->
		<div id="wrapper">
	

			<!-- Includes: Logo, loging links and Menu Bar -->
			<?php include 'header.inc';?>


			<!-- Contains Content Individual Item Wrapper -->
			<div id="contentindividualitem">
				<!-- Contains Item head, Item Description and Item Reviews -->
				<div id="contentindividualitemwrapper">
					
					<!-- Item Heading -->
					<div id="itemhead">
						<div id="itemheadwrapper">
							<img src="img/algester.png" alt="individual item photo"/> 
							<div id="itemheadtitle">

								<!-- Name changes dynamically --> 
								<?php 
									$venue = $_GET['VenueName'];
									echo "<p id=\"item-name\">$venue</p>";
								?>
																
								<?php 
									// Average Rating changes dynamically

										$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
									try { 
										$result = $pdo->query("SELECT Venue, round(AVG(Rating),0) AS AvgRating FROM reviews WHERE Venue='$venue' GROUP BY Venue ;");
									} catch (PDOException $e) {
										echo $e->getMessage(); 
									}

									$AvgRating = 0;

									if ($result->rowCount()==0){
										echo '<p id=\"rating-head\">Rating:&nbsp;<span id=\"item-rating\">No rating currently</span><p>';
									}else{
										foreach ($result as $row) {
											echo "<p id=\"rating-head\">Rating:&nbsp;<span id=\"item-rating\">$row[AvgRating]</span><p>";
											$AvgRating = $row['AvgRating'];
										}
									}

								?>
								<!--<p id="rating-head">Rating:&nbsp;<span id="item-rating">5</span><p>-->

							</div>
						</div><!--close itemheadwrapper-->
					</div><!--close itemhead-->


					<hr/>
					

					<!-- Item Information Details -->
					<div id="itemdescription">
						<div id="itemdescriptionwrapper">
							<!-- Left Side -->
							

							<!-- change this dynmically -->	
							<div id="contactdetails">
								<p id="contactdetailstitle">Contact Details</p>

							<?php
								$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								try { 
									$detail = $pdo->query("SELECT Suburb, Address, tennisCourts, BookingDetails, latitude, longitude FROM items WHERE Venue = '$venue'");
								} catch (PDOException $e) {
									echo $e->getMessage(); 
								}


								foreach ($detail as $det) { 
									echo "<table id=\"tablecontactdetails\">";
									echo "<tr>";
									echo "<td class='contactdetails'>Suburb: </td>";
									echo "<td>$det[Suburb]</td>";
									echo "</tr>";
									echo "<tr>";
									echo "<td class='contactdetails'>Address:&nbsp;</td>";
									echo "<td>$det[Address]</td>";
									echo "</tr>";
									echo "<tr>";
									echo "<td class='contactdetails'>Tennis Courts:&nbsp;</td>";
									echo "<td>$det[tennisCourts]</td>";
									echo "</tr>";
									echo "<tr>";
									echo "<td class='contactdetails'>Booking Details:&nbsp;</td>";
									echo "<td>$det[BookingDetails]</td>";
									echo "</tr>";					
									echo "</table>";	

									$Suburb = $det['Suburb'];
									$Address = $det['Address'];
									$BookingDetails = $det['BookingDetails'];
									$lat = $det['latitude'];
									$long = $det['longitude'];
									$venueNameStr = json_encode($venue);
									
								}
							?>

							</div>

							<!-- Right Side -->
							<div id="geolocation">
								<!-- using variables from previous query: latitutde and longitude -->
								<?php echo "<script> getLocation($venueNameStr,$lat,$long)</script>"?>
								<!--<script> getLocation()</script>-->
								<div id="mapholder"></div>
							</div>
						</div><!--close itemdescription wrapper-->
					</div><!--close itemdescription-->
					
					<br/><br/><br/><br/><br/>
					

					<hr/>
					

					<div id="itemreviews">
						<div id="itemsreviewswrapper">
							<div id="reviewsinput">
								<p id="itemsreviewstitle">My Review</p>
								<div id="myreview">


								<?php
									$alarm = '';
									if (isset($_POST['review-title']) && isset($_POST['review-text'])){
										//user already have filled two inputboxes
										if (!isset($_SESSION['username'])) {
											// ask user to login first
											$alarm = '<script>alert("Sorry, please login first");</script>';
											include 'reviewForm.inc';
										}else{
											// user already login and fill every input box
											$alarm = '<script>alert("SUCCESSFULLY!!!");</script>';

											$date = date('Y-m-d H:i:s');
											//echo "$date";

											$stmt = $pdo->prepare('INSERT INTO reviews (Email, Venue, Rating, Title, DateTime, ReviewText) VALUES (:email, :venue, :rating, :title,:datetime, :reviewtext);');
											$stmt->bindValue(':email', $_SESSION['username']);
											$stmt->bindValue(':venue', $venue);
											$stmt->bindValue(':rating', $_POST['rating']);
											$stmt->bindValue(':title', $_POST['review-title']);
											$stmt->bindValue(':datetime', $date);
											$stmt->bindValue(':reviewtext', $_POST['review-text']);

											$stmt->execute();
											header("location: individualitem.php?VenueName=$venue");
											//exit;
											include 'reviewForm.inc';
										}
									}else{
										// user didn't fill both of input boxes
										include 'reviewForm.inc';
									}
								?>

								</div>

							</div><!--close reviewsinput-->
							

							<div id="oldreviews">
								<p id="oldreviewstitle">Previous Reviews</p>
								
								<?php	
									$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
									try { 
										$result = $pdo->query("SELECT Email, Venue, Rating, Title, DateTime, ReviewText  FROM reviews WHERE Venue = '$venue'");
									} catch (PDOException $e) {
										echo $e->getMessage(); 
									}

									$RevCount = 0;
									$RevMinRating = 1;
									$RevMaxRating = 0;

									foreach ($result as $rev) { 

										echo "<table id=\"oldReviewsTable\">";
										
										echo '<tr>';
										echo "<td class=\"firstColumn\"> Rating: </td>";
										echo "<td class=\"secondColumn\">$rev[Rating]</td>";
										echo '</tr>';

										echo '<tr>';
										echo "<td class=\"firstColumn\">Title: </td>";
										echo "<td class=\"secondColumn\">$rev[Title]</td>";
										echo '</tr>';

										echo '<tr>';
										echo "<td class=\"firstColumn\">Date: </td>";
										echo "<td class=\"secondColumn\">$rev[DateTime]</td>";
										echo '</tr>';

										echo '<tr>';
										echo "<td class=\"firstColumn\">User: </td>";
										echo "<td class=\"secondColumn\">$rev[Email]</td>";
										echo '</tr>';

										echo '<tr>';
										echo "<td class=\"firstColumn\">Review: </td>";
										echo "<td class=\"secondColumn\">$rev[ReviewText]</td>";
										echo '</tr>';

										echo "</table>";
										echo "<br/>";

										$RevCount++;
										$RevRating = $rev['Rating'];
										$RevTitle = $rev['Title'];
										$RevDate = $rev['DateTime'];
										$RevAuthor = $rev['Email'];
										$RevBody = $rev['ReviewText'];
										

										if ($RevMinRating > $RevRating ){
											$RevMinRating = $RevRating;
										}

										if ($RevMaxRating < $RevRating ){
											$RevMaxRating = $RevRating;
										}

										echo "<div itemscope itemtype=\"http://schema.org/Review\">";
											echo "<span itemprop=\"itemReviewed\" content=\"$venue\"></span>";
											echo "<span itemprop=\"reviewBody\" content=\"$RevBody\"></span>";
											echo "<span itemprop=\"author\" content=\"$RevAuthor\"></span>";

											echo "<div itemprop=\"reviewRating\" itemscope itemtype=\"http://schema.org/AggregateRating\">";
												echo "<span itemprop=\"ratingValue\" content=\"$RevRating\"></span>";
												echo "<meta itemprop=\"reviewCount\" content=\"$RevCount\" />";
												echo "<meta itemprop=\"worstRating\" content=\"$RevMinRating\" />";
												echo "<meta itemprop=\"bestRating\" content=\"$RevMaxRating\" />";
											echo '</div>';
										echo '</div>';
		
									}
								?>
							</div><!--close oldreviews-->
						</div><!--close reviewswrapper-->
					</div><!--close reviews-->

				</div><!--close contentindividualitemwrapper-->
				<div id="bookMarkdiv"><a class="bookmark" href="#logo">Top of page</a></div>
				<br/>
			</div><!--close contentindividualitem-->
			<!--Footer-->
			<div id="footer">
				<p>Copyright &copy; 2016 JamZo CAB230 - Queensland University of Technology. All Rights Reserved</p>
			</div>
		</div><!--close wrapper-->
		<?php 
			if ($alarm!=''){
				echo $alarm;
				$alarm = '';
			}
		?>


		<?php
			// Geographical Data
			echo "<div itemscope itemtype=\"http://schema.org/Place\">";
				echo "<span itemprop=\"name\" content=\"$venue\"></span>";
				echo "<span itemprop=\"address\" content=\"$Address\"></span>";
				echo "<span itemprop=\"telephone\" content=\"$BookingDetails\"></span>";
			
				echo "<div itemscope itemtype=\"http://schema.org/PostalAddress\">";
					echo "<span itemprop=\"addressLocality\" content=\"$Suburb\"></span>";
				echo '</div>';
			
				echo "<div itemscope itemtype=\"http://schema.org/GeoCoordinates\">";
					echo "<meta itemprop=\"latitude\" content=\"$lat\" />";
					echo "<meta itemprop=\"longitude\" content=\"$long\"/>";
				echo '</div>';
			echo '</div>';
		?>

	</body>
</html>