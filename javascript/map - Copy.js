
var locations; // variable to store all of the location data from the database

// Main function for getting location
function getLocation(location)
{
    x=document.getElementById("MapException");

    locations = location; // get the all of data from php

    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
    }
    else{x.innerHTML="Geolocation is not supported by this browser.";}
}

function toRad(num){
    return num * Math.PI / 180;
}

/**
 * Calculate distance between two points on a sphere.
 */
function distance(lat1,lon1,lat2,lon2) {

    var R = 6371; // km
    var dLat = toRad(lat2-lat1);//(lat2-lat1).toRad();
    var dLon = toRad(lon2-lon1);//(lon2-lon1).toRad();
    var lat1 = toRad(lat1);//lat1.toRad();
    var lat2 = toRad(lat2);//lat2.toRad();
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d;
}

function arraySort(a) {
    for (var i = 0; i < a.length - 1; i++) {
        for (var j = 0; j < a.length - 1 - i; j++) {
            if (a[j] > a[j + 1]) {
                var tmp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = tmp;
            }
        }
    }
    return a;
}

// Show the location in the map
function showPosition(position)
{
    // Array for every location
    var markers = new Array();
    // Array containing all of latitude and longitude
    // var locations = [
    //     [-27.509708, 153.034015], [-27.427997, 153.053968],  [-27.615549, 153.030034], [-27.45843, 153.020306]
    // ];

    // Users' location
    lat=position.coords.latitude;
    lon=position.coords.longitude;

    // Calculate the distances
    var distances = new Array();
    var record = new Array();
    for(i = 0; i<locations.length; i++){
        distances.push(distance(lat, lon,locations[i][1], locations[i][2]));
    }
    //var old_distances = distances.concat();
    //distances = arraySort(distances);

    //for(i = 0; i<10; i++){
        //record.push(old_distances.indexOf(distances[i]));
    //}

    // Get the html element and set
    mapholder=document.getElementById('mapholder');
    mapholder.style.height='600px';
    mapholder.style.width='800px';

    latlon=new google.maps.LatLng(lat, lon);
    var myOptions={
        center:latlon,zoom:14,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false,
        navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    };
    var map=new google.maps.Map(document.getElementById("mapholder"),myOptions);

    // set local marker
    lat=position.coords.latitude;
    lon=position.coords.longitude;
    // set and add lat and lon
    latlon=new google.maps.LatLng(lat, lon);
    var marker = new google.maps.Marker({
        position: latlon,
        map: map,
        title: "You are here!"
    });
    markers.push(marker);


    var index = -1; // -1 represent users' location
    do{

        // set and add lat and lon
        // latlon=new google.maps.LatLng(lat, lon);
        //     var marker = new google.maps.Marker({
        //         position: latlon,
        //         map: map,
        //         title: locations[index][0]
        //     });
        // markers.push(marker);

        // Create the click event except users' locations
        // if( index!=-1 ){
        //     google.maps.event.addListener(markers[index+1], 'click', function(){
        //         window.location.href = "individualitem.php?venueName="+locations[index][0];
        //     })
        // }


        index++;
        if(index>=locations.length){
            break;
        }else{

            // if (contains(record, index)){
            //     lat = locations[index][1];
            //     lon = locations[index][2];
            // }else{
            //     continue;
            // }
            if(distances[index]<=10){

                lat = locations[index][1];
                lon = locations[index][2];

                latlon=new google.maps.LatLng(lat, lon);
                var marker = new google.maps.Marker({
                    position: latlon,
                    map: map,
                    title: locations[index][0]
                });

                markers.push(marker);
                google.maps.event.addListener(markers[index], 'click', function(){
                    window.location.href = "individualitem.php?VenueName="+locations[index][0];
                })





            }
        }
    }while(index<locations.length);

    

    // control the size of the map based on the locations
    var bounds = new google.maps.LatLngBounds();
    //  Go through each...
    for (var i = 0; i < markers.length; i++) {
        bounds.extend(markers[i].position);
    }
    //  Fit these bounds to the map
    map.fitBounds(bounds);

}

function contains(arr, obj) {
    var i = arr.length;
    while (i--) {
        if (arr[i] === obj) {
            return true;
        }
    }
    return false;
}

// The function for excpetion when loading the map
function showError(error)
{
    switch(error.code)
    {
        case error.PERMISSION_DENIED:
            x.innerHTML="User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML="Location information is unavailable.";
            break;
        case error.TIMEOUT:
            x.innerHTML="The request to get user location timed out.";
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML="An unknown error occurred.";
            break;
    }
}/**
 * Created by James on 2016/4/20.
 */

function showMap(id){
    var div1 = document.getElementById(id);
    
    if (div1.style.visibility != "visible"){
        div1.style.visibility = "visible";
    }else{
        div1.style.visibility = "hidden";
    }
   
}