//Login Form Validation Functions

function validateLoginForm() {

	var result = validateLoginEmail();
	result = validatePassword() && result;
	return result;
	
}


function validateLoginEmail(){
	//validating email
	var email = document.getElementById("login-email").value;

	if (email.indexOf('\'') > -1 || email.indexOf('\"') > -1 ) {
		alert( "Cannot include \' and \" in email" );
        return false;
    }
	if (email == null || email == ""){
        alert("Email must be filled out");
        return false;
    }else if(!/^[^@]+@[^@]+\.[a-zA-Z]{2,}$/.test(email)){
		alert("This is not Email format.");
        return false;
	}
	return true;
}

function validatePassword(){
	//validating password
	var pass1 = document.getElementById("login-password").value;
	
	if (pass1.indexOf('\'') > -1 || pass1.indexOf('"') > -1 ) {
		alert( "Cannot include \' and \" in password" );
        return false;
    }

	if (pass1 == null || pass1 == ""){
        alert("Password must be filled out");
        return false;
    }else if(!/[a-zA-Z0-9_-@]{8,16}/.test(pass1)){
		alert("Password can only inlucde letters, numbers and - _ @ symbols, and the length have to be between 8 and 16 characters");
        return false;
	}
	return true;

}


//Registration Form Validation functions

function validateRegistrationForm() {
	var result = validateFirstName();
	result = validateLastName() && result;
	result = validateBirthDate() && result;
	result = validateMobile() && result;
	result = validateGender() && result;
	result = validateEmail() && result;
	result = validatePassword1() && result;
	result = validatePassword2() && result;
	result = validateAgree() && result; 
	return result;
		
}

function validateBirthDate(){
	//validating first name
	var birthday = document.getElementById("register-birthday").value;
	
    if (birthday == null || birthday == ""){
        alert("Birthay must be filled out");
        return false;
    }else if(!/[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(birthday) && !/[0-9]{4}\/[0-9]{2}\/[0-9]{2}/.test(birthday) ){
		alert("The format of birthday should be (year)-(month)-(day) OR (year)/(month)/(day)");
        return false;
	}
	return true;
}

function validateMobile(){
	//validating first name
	var telephone = document.getElementById("register-telephone").value;
	
    if (telephone == null || telephone == ""){
        alert("Birthay must be filled out");
        return false;
    }else if(!/[0-9]{2}-[0-9]{4}-[0-9]{4}/.test(telephone)){
		alert("The format of telephone should be 00-0000-0000");
        return false;
	}
	return true;
}


function validateFirstName(){
	//validating first name
	var name = document.getElementById("register-firstname").value;
	
    if (name == null || name == ""){
        alert("Name must be filled out");
        return false;
    }else if(!/^[a-zA-Z]{4,}$/.test(name)){
		alert("Name should include at least 4 letters");
        return false;
	}
	return true;
}

function validateLastName(){
	//validating last name
	var lastname = document.getElementById("register-lastname").value;
    if (lastname == null || lastname == "") {
        alert("Last Name must be filled out");
        return false;
    }else if(!/^[a-zA-Z]{4,}$/.test(lastname)){
		alert("Last name should include at least 4 letters");
        return false;
	}
	return true;
}

function validateGender(){
		//validating gender
	var genderM = document.getElementById("register-gender-male");
	var genderF = document.getElementById("register-gender-female");

	if ( ( genderM.checked == false ) && ( genderF.checked == false ) ){
		alert("Please, you must select a gender");
		return false;
	} 
	return true;
}

function validateEmail(){
		//validating email
	var email = document.getElementById("register-email").value;

	if (email.indexOf('\'') > -1 || email.indexOf('\"') > -1 ) {
		alert( "Cannot include \' and \" in email" );
        return false;
    }
	if (email == null || email == ""){
        alert("Email must be filled out");
        return false;
    }else if(!/^[^@]+@[^@]+\.[a-zA-Z]{2,}$/.test(email)){
		alert("This is not Email format.");
        return false;
	}
	return true;
}

function validatePassword1(){
	//validating password
	var pass1 = document.getElementById("register-password").value;
	
	if (pass1.indexOf('\'') > -1 || pass1.indexOf('"') > -1 ) {
		alert( "Cannot include \' and \" in password" );
        return false;
    }

	if (pass1 == null || pass1 == ""){
        alert("Password must be filled out");
        return false;
    }else if(!/[a-zA-Z0-9\_\-\@]{8,16}/.test(pass1)){
		alert("Password can only inlucde letters, numbers and - _ @ symbols, and the length have to be between 8 and 16 characters");
        return false;
	}
	return true;

}

function validatePassword2(){
	//validating password confirmation
	var pass1 = document.getElementById("register-password").value;
	var pass2 = document.getElementById("register-password-repeat").value;
     if ( pass1 != pass2 ) {
		alert( 'Your password is not the same. Try again.' );
		return false;
	}
	return true;
}

function validateAgree(){
		//validating agreement checkbox
	var agree = document.getElementById("agree").value;
	if ( !agree ) {
		alert("You must agree with terms and conditions.");
		return false;
	}
	return true;
}