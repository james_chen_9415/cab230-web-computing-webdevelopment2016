var x = document.getElementById("demo");

var lat;
var lon;
var venueName;
// Main function for getting location
function getLocation(name,x,y) {
    venueName = name;
	lat = x;
	lon = y;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

// Show the location in the map
function showPosition(position) {
    // Get the users' locations
    // lat = position.coords.latitude;
    // lon = position.coords.longitude;
    latlon = new google.maps.LatLng(lat, lon);

    // Get the element in the html and set it
    mapholder = document.getElementById('mapholder');
    mapholder.style.height = '250px';
    mapholder.style.width = '500px';
    var myOptions = {
    center:latlon,zoom:14,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    mapTypeControl:false,
    navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    };
    var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);

    // Put marker in the map
    var marker = new google.maps.Marker({position:latlon,map:map,title:venueName});
}

// The function for excpetion when loading the map
function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable.";
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out.";
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred.";
            break;
    }
}