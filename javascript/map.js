
var locations;

// Main function for getting location
function getLocation(location)
{
    x=document.getElementById("MapException");

    locations = location;

    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
    }
    else{x.innerHTML="Geolocation is not supported by this browser.";}
}

function toRad(num){
    return num * Math.PI / 180;
}

/**
 * Calculate distance between two points on a sphere.
 */
function distance(lat1,lon1,lat2,lon2) {

    var R = 6371; // km
    var dLat = toRad(lat2-lat1);//(lat2-lat1).toRad();
    var dLon = toRad(lon2-lon1);//(lon2-lon1).toRad();
    var lat1 = toRad(lat1);//lat1.toRad();
    var lat2 = toRad(lat2);//lat2.toRad();
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d;
}

// Show the location in the map
function showPosition(position) {
    // Array for every location
    var markers = new Array();
    // Array containing all of latitude and longitude

    // Users' location
    lat=position.coords.latitude;
    lon=position.coords.longitude;

    // Calculate the distances
    var distances = new Array();
    for(i = 0; i<locations.length; i++){
        distances.push(distance(lat, lon,locations[i][1], locations[i][2]));
    }

    //alert(locations[3][1]);

    // Get the html element and set
    mapholder=document.getElementById('mapholder');
    mapholder.style.height='600px';
    mapholder.style.width='800px';
    latlon=new google.maps.LatLng(lat, lon);
    var marker = new google.maps.Marker({
        position: latlon,
        map: map,
        title: 'you are here'
    });
    var myOptions={
        center:latlon,zoom:14,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false,
        navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    };
    var map=new google.maps.Map(document.getElementById("mapholder"),myOptions);

    markers.push(marker);

    // google.maps.event.addListener(markers[0], 'click', function(){
    //     window.location.href = "individualitem.php?";
    // });

    venueIndex = 0;
    var nameArr = new Array();
    markerIndex = 0;
    var index = -1; // -1 represent users' location
    do{


        index++;
        if(index>=locations.length){
            break;
        }else{
            if(distances[index]<=10){
                lat = locations[index][1];
                lon = locations[index][2];

                //alert(locations[index][0]);
                nameArr.push(locations[index][0]);
                // set and add lat and lon
                latlon=new google.maps.LatLng(lat, lon);
                var marker = new google.maps.Marker({
                    position: latlon,
                    map: map,
                    title: nameArr[venueIndex]
                });
                markers.push(marker);

                google.maps.event.addListener(markers[++markerIndex], 'click', (function(nameArr, venueIndex){
                     return function() {
                    window.location.href = "individualitem.php?VenueName="+nameArr[venueIndex];
                     }
                })(nameArr,venueIndex));
                venueIndex++;
            }else{
                continue;
            }

        }
    }while(index<locations.length);


    // control the size of the map based on the locations
    var bounds = new google.maps.LatLngBounds();
    //  Go through each...
    for (var i = 0; i < markers.length; i++) {
        bounds.extend(markers[i].position);
    }
    //  Fit these bounds to the map
    map.fitBounds(bounds);


}



// The function for excpetion when loading the map
function showError(error)
{
    switch(error.code)
    {
        case error.PERMISSION_DENIED:
            x.innerHTML="User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML="Location information is unavailable.";
            break;
        case error.TIMEOUT:
            x.innerHTML="The request to get user location timed out.";
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML="An unknown error occurred.";
            break;
    }
}/**
 * Created by James on 2016/4/20.
 */


/**
 * Created by James on 2016/4/20.
 */

function showMap(id){
    var div1 = document.getElementById(id);

    if (div1.style.visibility != "visible"){
        div1.style.visibility = "visible";
    }else{
        div1.style.visibility = "hidden";
    }

}