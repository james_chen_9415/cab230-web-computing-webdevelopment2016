<?php 
	include 'sessionStart.inc';
?>

<!DOCTYPE HTML>
<html>
	
	<head>
		<!-- metadata -->
		<meta charset = "UTF-8">
		<meta name="description" content="Search tennis courts around Brisbane according to name, suburb or rating." />
		<meta name="keywords" content="tennis, courts, brisbane, suburb, rating, sports" />
		<meta name="author" content="Renzo Alvarado and Jiaming Chen">
		<meta name="robots" content="noindex, nofollow">
		<title>Search</title>
		<!-- External CSS -->
		<link href="css/index_style.css" rel="stylesheet" type="text/css"/>
		<link href="css/content_results_style.css" rel="stylesheet" type="text/css"/>
		<!-- External JavaScript-->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="javascript/map.js"></script>
	</head>
	
	<body>
	
		<!-- Contains: Header, ContentSearch and Footer -->
		<div id="wrapper">
	
			<!-- Includes: Logo, loging links and Menu Bar -->
			<?php include 'header.inc';?>
			
			
			<!-- Contains: TopArea and BottomArea-->
			<div id="contentsearch">
				<!-- Contains: TopAreaWrapper-->
				<div id="toparea">
					<!-- Contains: FinderIntro and FinderInput-->
					<div id="topareawrapper">
						<br/><br/>
						<p id="finderintro">
							Find information about tennis courts for community use in Brisbane parks, tennis clubs and schools.
							
						</p>
							<?php 
								// finderinput div
								include 'searchForm.inc';
							?>
							<span id="geolocationInstruction">Near me:<input type="checkbox" name="geolocation" onclick="showMap('mapholder');" ></span>
					</div><!--close topareawrapper-->	
				</div><!--close toparea-->	
				<br/>
				<hr/>
				<!-- Contains: BottomAreaWrapper-->
				<div id="bottomarea" style="text-align: center">
					<!-- Contains: Sidebar and Results-->
					<div id="bottomareawrapper" >
						
						<!--<div id="sidebar">-->
							<!--&lt;!&ndash; start feedwind code &ndash;&gt;<script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');</script><script type="text/javascript">(function() {var params = {rssmikle_url: "http://www.atpworldtour.com/en/media/rss-feed/xml-feed",rssmikle_frame_width: "240",rssmikle_frame_height: "400",frame_height_by_article: "5",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "11",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "on",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "20",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "ATP World Tour",rssmikle_title_link: "http://www.atpworldtour.com/en/",rssmikle_title_bgcolor: "#1C5A04",rssmikle_title_color: "#FFFFFF",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "30",rssmikle_item_title_color: "#4DB301",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "120",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%e.%m.%Y %l:%M %p",item_description_style: "text+tn",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "10",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();</script><div style="font-size:10px; text-align:center; width:240px;"><a href="http://feed.mikle.com/" target="_blank" style="color:#CCCCCC;">RSS Feed Widget</a>&lt;!&ndash;Please display the above link in your web page according to Terms of Service.&ndash;&gt;</div>&lt;!&ndash; end feedwind code &ndash;&gt;&lt;!&ndash;  end  feedwind code &ndash;&gt;-->
						<!--</div>-->
						
						 <div id="results">
							<div id="MapException"> </div>

							<?php 

								$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								try { 
									$detail = $pdo->query("SELECT Venue, latitude, longitude FROM items;");
								} catch (PDOException $e) {
									echo $e->getMessage(); 
								}

								$locations = array();
								foreach($detail as $det){
									$tempArr = array($det['Venue'], $det['latitude'], $det['longitude']);
									array_push($locations,$tempArr);
								}
								$strLocations = json_encode($locations);

								echo "<script type=\"text/javascript\">getLocation($strLocations);</script>";
							?>
							
							<div id="mapholder"></div>
							<!--this script is run when click checkbox near me-->
							<!--<script type="text/javascript"> getLocation()</script>-->
						</div> <!--close results -->
					
					</div> <!--close bottomareawrapper-->
				
				</div> <!--close bottomarea-->
				<p><a href="#logo" class="bookmark">Top of page</a></p>	
				
			</div> <!--close contentsearch-->	
			
			<br>

			<div id="footer">
				<p>Copyright &copy; 2016 JamZo CAB230 - Queensland University of Technology. All Rights Reserved</p>
			</div>
		
		</div><!--close wrapper-->

	</body>
</html>