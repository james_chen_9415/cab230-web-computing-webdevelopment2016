<!DOCTYPE HTML>

<html>
	
	<head>
		<meta charset = "UTF-8">
		<title>Item</title>
		<!-- External CSS -->
		<link href="css\index_style.css" rel="stylesheet" type="text/css"/>
		<link href="css\content_individualitem_style.css" rel="stylesheet" type="text/css"/>
		<!-- External Scripts-->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script src="javascript\individualitem-map.js"></script>
	</head>
	
	<body>
	<?php $pdo = new PDO('mysql:host=localhost;dbname=tenniscourt', 'Jamzo', 'Renzo1981James1994'); ?>
		<!-- Contains: Header, Content Individual Item and Footer -->
		<div id="wrapper">
	
			<!-- Contains: HeaderTop and HeaderMenu-->
			<div id="header">
				<!--Contains: Logo and LoginLinks-->
				<div id="headertop">
					<a href="index.html"><img id="logo" src="img/logofinal.png" alt="logo" /></a>
					<div id="loginlinks">
						<?php 
							session_start();
							if(isset($_SESSION['username'])){
								echo "<div id=\"login\">$_SESSION[username]</div>";
							}
							else{
								echo '<div id="login"><a href="login.html">Login</a> | <a href="register.html">Register</a></div>';
							}
						?>
						<!--<div id="login"><a href="login.html">Login</a> | <a href="register.html">Register</a></div>-->
					</div>
				</div>
				<!--Contains: MenuOptions-->
				<div id="headermenu"><a href="index.html">HOME</a><a href="search.html">SEARCH</a><a href="events.html">ABOUT</a><a href="shops.html">FAQ</a></div>
			</div><!-- Close Header-->
			<!-- Contains Content Individual Item Wrapper -->
			<div id="contentindividualitem">
				<!-- Contains Item head, Item Description and Item Reviews -->
				<div id="contentindividualitemwrapper">
					<!-- Item Heading -->
					<div id="itemhead">
						<div id="itemheadwrapper">
							<img src="img/algester.png" alt="individual item photo"/> 
							<div id="itemheadtitle">
								<p id="item-name">Algester State School</p>
								<p id="rating-head">Rating:&nbsp;<span id="item-rating">5</span><p>
							</div>
						</div><!--close itemheadwrapper-->
					</div><!--close itemhead-->
					<hr/>
					<!-- Item Information Details -->
					<div id="itemdescription">
						<div id="itemdescriptionwrapper">
							<!-- Left Side -->
							<div id="contactdetails">
								<p id="contactdetailstitle">Contact Details</p>
								<table id="tablecontactdetails">
									<tr>
										<td>Phone: &nbsp;</td>
										<td>07 3712 5111</td>
									</tr>
									<tr>
										<td>Address:&nbsp;</td>
										<td>19 Endiandra Street, Algester</td>
									</tr>
									<tr>
										<td>Email:&nbsp;</td>
										<td></td>
									</tr>					
								</table>					
							</div>
							<!-- Right Side -->
							<div id="geolocation">
								<script> getLocation()</script>
								<div id="mapholder"></div>
							</div>
						</div><!--close itemdescription wrapper-->
					</div><!--close itemdescription-->
					<br/><br/><br/><br/><br/>
					<hr/>
					<div id="itemreviews">
						<div id="itemsreviewswrapper">
							<div id="reviewsinput">
								<p id="itemsreviewstitle">My Review</p>
								<div id="myreview">


								<?php
									$alarm = '';
									if (isset($_POST['review-title']) && isset($_POST['review-text'])){
										//user already have filled two inputboxes
										if (!isset($_SESSION['username'])) {
											// ask user to login first
											$alarm = '<script>alert("Sorry, please login first");</script>';
											include 'reviewForm.inc';
										}else{
											// user already login and fill every input box
											// do mysql now
											$alarm = '<script>alert("SUCCESSFULLY!!!");</script>';
											

											$date = date('Y-m-d H:i:s');
											echo $date;


											$stmt = $pdo->prepare('INSERT INTO reviews (Email, Venue, Rating, Title, DateTime, ReviewText) VALUES (:email, :venue, :rating, :title,:datetime, :reviewtext);');
											$stmt->bindValue(':email', $_SESSION['username']);
											$stmt->bindValue(':venue', 'Algester State School');
											$stmt->bindValue(':rating', $_POST['rating']);
											$stmt->bindValue(':title', $_POST['review-title']);
											$stmt->bindValue(':datetime', $date);
											$stmt->bindValue(':reviewtext', $_POST['review-text']);

											$stmt->execute();
										}
									}else{
										// user didnot fill both of inputboxes
										include 'reviewForm.inc';
									}
								?>

								</div>
								<!--<a href="results.html"><button type="button" onclick="">Submit</button></a>-->


							</div><!--close reviewsinput-->
							

							<div id="oldreviews">
								<p id="oldreviewstitle">Previous Reviews</p>
								
								<table>
								<tr>
								<td>Rating:</td>	
								<td>$rating</td>
								</tr>

								<tr>
								<td>Title:</td>	
								<td>$title</td>
								</tr>

								<tr>
								<td>User:</td>	
								<td>$user</td>
								</tr>

								<tr>
								<td>Review:</td>	
								<td>$review</td>
								</tr>

								</table>	

							</div><!--close oldreviews-->




							 <!-- <div id="oldreviews">
								<p id="oldreviewstitle">Previous Reviews</p>
								Rating: <span id="rating">5</span>
								<p id="username">James</p>
								<p id="review-title-old">It's Perfect!</p>
								<p id="review-text">It is perfect!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</p>
							</div><!--close oldreviews  -->
						



						</div><!--close reviewswrapper-->
					</div><!--close reviews-->
				</div><!--close contentindividualitemwrapper-->
				<p><a class="bookmark" href="#logo">Top of page</a></p>
			</div><!--close contentindividualitem-->
			<!--Footer-->
			<div id="footer">
				<p>Copyright &copy; 2016 JamZo CAB230 - Queensland University of Technology. All Rights Reserved</p>
			</div>
		</div><!--close wrapper-->
		<?php 
			if ($alarm!=''){
				echo $alarm;
				$alarm = '';
			}
		?>
	</body>
</html>