
<!-- This form is divided in two divs: registration box left and registration box right -->
<form name="registrationForm" id="registrationForm" method="POST" action="register.php">
	<!-- Form part 1: put personal details -->
	<div id="registerBoxLeft">
		<p class="subtitle">Personal Details</p>
		
		<label class="inputlabel" for="register-firstname">First Name</label><span class="alert">&#42;</span>
		<input type="text" name="user_firstname" id="register-firstname" placeholder=" Enter your name" value="<?php if(isset($_POST['user_firstname'])) echo validateNotation($_POST['user_firstname'])?>" >

		<?php 
		if (array_key_exists("user_firstname", $RegisterErrors)) {
			echo "<span class=\"alert\"> $RegisterErrors[user_firstname] </span>" ;
		 }
		?>	

		<br><br>
			
		<label class="inputlabel" for="register-lastname">Last Name</label><span class="alert" >&#42;</span>
		<input type="text" name="user_lastname" id="register-lastname" placeholder=" Enter your last name" value="<?php if(isset($_POST['user_lastname'])) echo validateNotation($_POST['user_lastname'])?>">

		<?php 
		if (array_key_exists("user_lastname", $RegisterErrors)) {
		 	echo "<span class=\"alert\"> $RegisterErrors[user_lastname] </span>" ;
		 }
		?>

		<br><br>
			
		<label class="inputlabel" for="register-birthday">Date of Birth</label><span class="alert" >&#42;</span>
		<input type="text" name="user_birthday" id="register-birthday" placeholder=" Date of birth" value="<?php if(isset($_POST['user_birthday'])) echo validateNotation($_POST['user_birthday'])?>">
		<?php 
		if (array_key_exists("user_birthday", $RegisterErrors)) {
		 	echo "<span class=\"alert\"> $RegisterErrors[user_birthday] </span>" ;
		 }
		?>

		<br><br>
			
		<label class="inputlabel" for="register-gender-male">Gender</label><span class="alert">&#42;</span>
		<input type="radio" name="user_gender" id="register-gender-male" value="1" checked>Male 
		<input type="radio" name="user_gender" id="register-gender-female" value="0">Female
		<br><br>
			
		<label class="inputlabel" for="register-telephone">Telephone</label><span class="alert" >&#42;</span>
		<input type='tel' name="user_telephone" id="register-telephone" placeholder=" Enter your phone number"  title='Phone Number (Format: 99-9999-9999)' value="<?php if(isset($_POST['user_telephone'])) echo validateNotation($_POST['user_telephone'])?>">
		
		<?php 
		if (array_key_exists("user_telephone", $RegisterErrors)) {
		 	echo "<span class=\"alert\"> $RegisterErrors[user_telephone] </span>" ;
		 }
		?>
	</div> 
	
	<!-- Form part 2: put email address and password -->
	<div id="registerBoxRight">
		<p class="subtitle">Login  Details</p>
		<label class="inputlabel" for="register-email">Email</label><span class="alert">&#42;</span>
		<input type="text" name="register_user_email" id="register-email" placeholder=" email@address.com" title="Make sure you enter in an @" value="<?php if(isset($_POST['register_user_email'])) echo validateNotation($_POST['register_user_email'])?>">
		<?php 
		if (array_key_exists("register_user_email", $RegisterErrors)) {
		 	echo "<span class=\"alert\"> $RegisterErrors[register_user_email] </span>" ;
		 }
		?>
		<br><br>
		
		<label class="inputlabel" for="register-password">Password</label><span class="alert">&#42;</span>
		<input type="password" name="register_user_password" id="register-password" placeholder=" Enter your password">
		<?php 
		if (array_key_exists("register_user_password", $RegisterErrors)) {
		 	echo "<span class=\"alert\"> $RegisterErrors[register_user_password] </span>" ;
		 }
		?>
		<br><br>
		
		<label class="inputlabel" for="register-password-repeat">Confirm Password</label><span class="alert">&#42;</span>
		<input type="password" name="user_password_repeat" id="register-password-repeat" placeholder=" Confirm your password">
		<?php 
		if (array_key_exists("user_password_repeat", $RegisterErrors)) {
		 	echo "<span class=\"alert\"> $RegisterErrors[user_password_repeat] </span>" ;
		 }
		?>
		<br><br><br>
		
		<!-- Agree link and Submit -->
		<input type="checkbox" onclick="" name="agree" class="agreeTerms" id="agree" required>I agree with <a href="#"> terms and conditions</a>&nbsp;
		<input type="submit" value="Register" class="submit" onClick="return validateRegistrationForm();">
		<br><br>
		<!-- <span class="alert" id="alertLogIn">&#42; Required</span> -->
		
	</div>
</form><!-- Close registrationForm -->