<?php 
	include 'sessionStart.inc';
?>

<!DOCTYPE HTML>

<html>
	
	<head>
		
		<!-- metadata -->
		<meta charset = "UTF-8">
		<meta name="description" content="A website that helps people to find tennis courts around Brisbane." />
		<meta name="keywords" content="tennis, courts, brisbane, council, play, sports" />
		<meta name="author" content="Renzo Alvarado and Jiaming Chen">
		<meta name="robots" content="noindex, nofollow">
		<title>TennisFinder</title>
		<!-- External CSS -->
		<link href="css/index_style.css" rel="stylesheet" />
		<link href="css/content_index_style.css" rel="stylesheet" type="text/css"/>
	</head>
	
	<body>
	
		<!-- Contains: Header, ContentIndex and Footer -->
		<div id="wrapper">
			
			<!-- Includes: Logo, loging links and Menu Bar -->
			<?php include 'header.inc'; ?>

						
			<!-- Contains: TopArea and BottomArea-->
			<div id="contentindex">
			
				<!-- Contains: TopAreaWrapper-->
				<div id="toparea">
					<!-- Contains: ContentPicture and FinderInput-->
					<div id="topareawrapper">
						
						<div id="contentpicture">
							<img id="photohome" src="img/nadal.png" alt="nadal"/>
							<div id="transparentbox"></div>
							<p id="firstline"> Be the Best</p>
							<p id="secondline"> Play in the best tennis courts of Brisbane. <br/><br/>
							We are proud to have the most beautiful tennis courts in the country, <br/>
							so what are you waiting for?!</p>
						</div>
						
						<p id="finderintro"> Find information about tennis courts for community use in Brisbane parks, tennis clubs and schools.</p>
						
						<?php 
							include 'searchForm.inc';
						?>
						
					</div> <!-- close topareawrapper -->
				</div> <!-- close toparea -->

				<hr/>				
				<!-- Contains: BottomAreaWrapper -->
				<div id="bottomarea">
					<!-- Contains: Sidebar and AdvertisementBox-->
					<div id="bottomareawrapper">
						<div id="sidebar"><!-- imported rss -->
							<!-- start feedwind code --><script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');</script><script type="text/javascript">(function() {var params = {rssmikle_url: "http://www.atpworldtour.com/en/media/rss-feed/xml-feed",rssmikle_frame_width: "240",rssmikle_frame_height: "400",frame_height_by_article: "5",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "11",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "on",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "20",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "ATP World Tour",rssmikle_title_link: "http://www.atpworldtour.com/en/",rssmikle_title_bgcolor: "#1C5A04",rssmikle_title_color: "#FFFFFF",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "30",rssmikle_item_title_color: "#4DB301",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "120",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%e.%m.%Y %l:%M %p",item_description_style: "text+tn",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "10",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();</script><div style="font-size:10px; text-align:center; width:240px;"><a href="http://feed.mikle.com/" target="_blank" style="color:#CCCCCC;">RSS Feed Widget</a><!--Please display the above link in your web page according to Terms of Service.--></div><!-- end feedwind code --><!--  end  feedwind code -->
						</div> 
						<div id="advertisement-box">
							<p id="topsheading">Brisbane's most popular Tennis Courts 2016</p>
							<div id="ads">
								<span id="adtop1">
									<p>Top # 1</p>
									<img src="img/ad1.png" alt="advertisement1"/>
									<p><a href="individualitem.php?VenueName=Algester State School" class="topdescription">ALGESTER STATE SCHOOL</a></p>
								</span>
								<span id="adtop2">
									<p>Top # 2</p>
									<img src="img/ad2.png" alt="advertisement2"/>
									<p><a href="individualitem.php?VenueName=Carina State School" class="topdescription">CARINA STATE SCHOOL</a></p>
								</span>
							</div>
						</div>
					</div><!--Closes BottomAreaWrapper-->
				</div><!--Closes BottomArea-->
				<p><a class="bookmark" href="#logo">Top of page</a></p>
			</div><!--Close ContentIndex-->
			
			
			
			<div id="footer">
				<p>Copyright &copy; 2016 JamZo CAB230 - Queensland University of Technology. All Rights Reserved</p>
			</div>
			
		</div><!--Close wrapper-->
	</body>
</html>