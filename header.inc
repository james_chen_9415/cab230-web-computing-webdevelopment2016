<!-- Contains: HeaderTop and HeaderMenu-->
			<div id="header">
				<!--Contains: Logo and LoginLinks-->
				<div id="headertop">
					<a href="index.php"><img id="logo" src="img/logofinal.png" alt="logo" /></a>
					<div id="loginlinks">
						<?php 
							if(isset($_SESSION['username'])){
								echo "<div id=\"login\">$_SESSION[username]| <a href=\"logout.php\">Logout</a></div>";
							}
							else{
								echo '<div id="login"><a href="register.php">Login</a> | <a href="register.php">Register</a></div>';
							}
						?>
					</div>
				</div>
				<!--Contains: MenuOptions-->
				<div id="headermenu">
					<a id="homelink" href="index.php">HOME</a>
					<a href="search.php">SEARCH</a>
					<a href="aboutUs.php">ABOUT</a>
					<a href="faq.php">FAQ</a>
				</div>
			</div><!-- Close Header-->